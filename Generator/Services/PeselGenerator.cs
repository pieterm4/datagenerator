﻿using System;
using System.Linq;
using System.Text;

namespace Generator.Services
{
    public class PeselGenerator
    {
        private readonly Random _random;


        public PeselGenerator()
        {
            _random = new Random();
        }


        public string Generate()
        {
            var peselStringBuilder = new StringBuilder();
            var birthDate = DateGenerator.Generate(1900, 2099);
            AppendPeselDate(birthDate, peselStringBuilder);
            peselStringBuilder.Append(GenerateRandomNumbers(4));
            peselStringBuilder.Append(PeselCheckSumCalculator.Calculate(peselStringBuilder.ToString()));

            return peselStringBuilder.ToString();
        }


        public static string GetPeselMonthShiftedByYear(DateTime date)
        {
            if (date.Year < 1900 || date.Year > 2299)
                throw new NotSupportedException($"PESEL for year: {date.Year} is not supported");

            var monthShift = (date.Year - 1900) / 100 * 20;

            return (date.Month + monthShift).ToString("00");
        }


        private void AppendPeselDate(DateTime date, StringBuilder builder)
        {
            builder.Append((date.Year % 100).ToString("00"));
            builder.Append(GetPeselMonthShiftedByYear(date));
            builder.Append(date.Day.ToString("00"));
        }


        private string GenerateRandomNumbers(int numbersCount)
        {
            var maxValue = (int) Math.Pow(10, numbersCount);
            var format = "D" + numbersCount;

            return _random.Next(maxValue).ToString(format);
        }
    }

    public class PeselCheckSumCalculator
    {
        private static readonly int[] _Weight = {1, 3, 7, 9, 1, 3, 7, 9, 1, 3};

        public static int Calculate(string pesel)
        {
            var checkSum = pesel.Zip(_Weight, (digit, weight) => (digit - '0') * weight)
                .Sum();

            var lastDigit = checkSum % 10;

            return lastDigit == 0 ? 0 : 10 - lastDigit;
        }
    }
}