﻿using System.Threading.Tasks;

namespace Generator.Services
{
    public interface IGetDataService<T> where T : class
    {
        Task<T> GetData(string path);
    }
}