﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Generator.Services
{
    public class GetDataFromText : IGetDataService<List<string>>
    {
        public async Task<List<string>> GetData(string path)
        {
            var elements = new List<string>();
            try
            {
                using (var streamReader = new StreamReader(path))
                {
                    string line;
                    while ((line = await streamReader.ReadLineAsync()) != null)
                    {
                        elements.Add(line);
                        Console.WriteLine(line);
                    }
                }

                return elements;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}