﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Generator.Services
{
    public class GetDataFromJson<T> : IGetDataService<List<T>>
    {
        public GetDataFromJson()
        {
            Elements = new List<T>();
        }

        public List<T> Elements { get; set; }

        public async Task<List<T>> GetData(string path)
        {
            try
            {
                using (var reader = new StreamReader(path))
                {
                    var json = await reader.ReadToEndAsync();
                    var converted = JsonConvert.DeserializeObject<List<T>>(json);

                    return Elements = new List<T>(converted);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}