﻿using System;

namespace Generator.Services
{
    public static class DateGenerator
    {
        private static readonly Random Random = new Random();

        public static DateTime Generate(int yearFrom, int yearTo)
        {
            var year = Random.Next(yearFrom, yearTo + 1);
            var month = Random.Next(12) + 1;
            var day = Random.Next(DateTime.DaysInMonth(year, month)) + 1;
            var hour = DateTime.Now.Hour;
            var min = DateTime.Now.Minute;
            var sec = DateTime.Now.Second;

            return new DateTime(year, month, day, hour, min, sec);
        }
    }
}