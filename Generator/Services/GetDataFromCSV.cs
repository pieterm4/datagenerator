﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Generator.Model;

namespace Generator.Services
{
    public class GetDataFromCSV : IGetDataService<List<Polozenie>>
    {
        public async Task<List<Polozenie>> GetData(string path)
        {
            var polozenia = new List<Polozenie>();
            try
            {
                using (var str = new StreamReader(path))
                {
                    var i = 0;
                    while (!str.EndOfStream)
                    {
                        var line = await str.ReadLineAsync();
                        var value = line.Split(',');

                        var miasto = value[2];
                        var woj = value[3];

                        var polozenie = new Polozenie(woj, $"Powiat {i}", $"Gmina {i}", miasto, $"Ulica {i}",
                            69.ToString(), 75.ToString());
                        polozenia.Add(polozenie);
                        i++;
                    }
                }

                return polozenia;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}