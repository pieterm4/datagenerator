﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Generator.Generate;

namespace Generator
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var task = Task.Run(Generate);
            task.Wait();
            Console.ReadKey();
        }

        private static async Task Generate()
        {
            var generatory = new List<IGenerator>
            {
                new GeneratorPolozenie(),
                new GeneratorKartaSim(),
                new GeneratorPolozenie(),
                new GeneratorKlient(),
                new GeneratorTelefon(),
                new GeneratorNadajnik(),
                new GeneratorAwarie(),
                new GeneratorPolaczenia()
            };

            foreach (var generator in generatory)
                await generator.Generate();
        }
    }
}