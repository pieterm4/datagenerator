﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Generator.DBConnection;
using Generator.Model;
using Generator.Services;

namespace Generator.Generate
{
    public class GeneratorAwarie : IGenerator
    {
        private readonly SqlConnection _sqlConnecition;

        public GeneratorAwarie()
        {
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
        }

        public async Task Generate()
        {
            try
            {
                using (_sqlConnecition)
                {
                    await _sqlConnecition.OpenAsync();

                    for (var i = 1; i <= 10000; i++)
                        if (i % 5 == 0 || i % 100 == 0)
                        {
                            var awaria = new Awarie(i, DateGenerator.Generate(1980, 2017), "Jakas uwaga");

                            var sqlCommand =
                                new SqlCommand(
                                    $"INSERT INTO Awarie VALUES ({awaria.IdNadajnika}, '{awaria.Data}', '{awaria.Uwagi}')",
                                    _sqlConnecition);
                            await sqlCommand.ExecuteNonQueryAsync();
                            Console.WriteLine(awaria);
                        }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}