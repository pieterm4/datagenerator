﻿using System.Threading.Tasks;

namespace Generator.Generate
{
    public interface IGenerator
    {
        Task Generate();
    }
}