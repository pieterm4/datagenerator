﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Generator.DBConnection;

namespace Generator.Generate
{
    public class GeneratorKartaSim : IGenerator
    {
        private readonly Random _random;

        private readonly SqlConnection _sqlConnecition;

        public GeneratorKartaSim()
        {
            _random = new Random();
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
        }

        public async Task Generate()
        {
            try
            {
                await _sqlConnecition.OpenAsync();
                using (_sqlConnecition)
                {
                    for (var i = 0; i < 1000000; i++)
                    {
                        var msi = _random.Next(0, 10000000).ToString();
                        var nrTelefonu = $"+48 {msi}";

                        var sqlCommand =
                            new SqlCommand($"INSERT INTO KartaSim VALUES ({msi}, '{nrTelefonu}')", _sqlConnecition);
                        await sqlCommand.ExecuteNonQueryAsync();
                        Console.WriteLine($"{msi}, {nrTelefonu}");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}