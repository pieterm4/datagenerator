﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Generator.DBConnection;
using Generator.Model;
using Generator.Services;

namespace Generator.Generate
{
    public class GeneratorKlient : IGenerator
    {
        private const string NamesFile = @"C:\Users\piotr\Desktop\eh\Generator\Model\Imiona.txt";
        private const string SurnamesFile = @"C:\Users\piotr\Desktop\eh\Generator\Model\Nazwiska.txt";
        private readonly GetDataFromText _getDataFromText;
        private readonly PeselGenerator _peselGenerator;
        private readonly Random _random;
        private readonly SqlConnection _sqlConnecition;

        public GeneratorKlient()
        {
            _peselGenerator = new PeselGenerator();
            _random = new Random();
            _getDataFromText = new GetDataFromText();
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
        }

        public async Task Generate()
        {
            try
            {
                var names = await _getDataFromText.GetData(NamesFile);
                var surnames = await _getDataFromText.GetData(SurnamesFile);

                using (_sqlConnecition)
                {
                    await _sqlConnecition.OpenAsync();

                    for (var i = 1; i <= 1000000; i++)
                    {
                        var pesel = _peselGenerator.Generate();
                        var imie = names.ElementAt(_random.Next(0, names.Count - 1));
                        var nazwisko = surnames.ElementAt(_random.Next(0, surnames.Count - 1));
                        var adres = i;
                        var klient = new Klient(pesel, imie, nazwisko, adres);


                        var sqlCommand =
                            new SqlCommand(
                                $"Insert into Klient (Pesel, Imie, Nazwisko, IdAdresu) VALUES ('{klient.Pesel}', '{klient.Imie}', '{klient.Nazwisko}', '{klient.Adres}')",
                                _sqlConnecition);

                        await sqlCommand.ExecuteNonQueryAsync();

                        Console.WriteLine($"Inserted: {klient}");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}