﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Generator.DBConnection;
using Generator.Model;
using Generator.Services;

namespace Generator.Generate
{
    public class GeneratorTelefon : IGenerator
    {
        private const string Path = "C:\\Users\\piotr\\Desktop\\eh\\Generator\\Model\\phones.json";
        private readonly Random _random;
        private readonly GetDataFromJson<PhoneNames> _service;
        private readonly SqlConnection _sqlConnecition;

        public GeneratorTelefon()
        {
            _service = new GetDataFromJson<PhoneNames>();
            _random = new Random();
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
        }

        public async Task Generate()
        {
            try
            {
                var phoneNames = await _service.GetData(Path);

                using (_sqlConnecition)
                {
                    await _sqlConnecition.OpenAsync();

                    for (var i = 1; i <= 1000000; i++)
                    {
                        var imei = _random.Next(0, 1000000).ToString();
                        var index = _random.Next(0, phoneNames.Count - 1);
                        var nazwa = phoneNames.ElementAt(index).vendor;
                        var model = phoneNames.ElementAt(index).model;
                        var idKlienta = i + 1;
                        var idSim = i;

                        var sqlCommand = new SqlCommand(
                            $"INSERT INTO Telefon (Imei, Nazwa, Model, DataZakupu, IdKlienta, IdSim) VALUES ('{imei}', '{nazwa}', '{model}', '{DateGenerator.Generate(1980, 2017)}', '{idKlienta}', '{idSim}')",
                            _sqlConnecition);
                        await sqlCommand.ExecuteNonQueryAsync();

                        Console.WriteLine(
                            $"{i}: ('{imei}', '{nazwa}', '{model}', '{idSim}', '{DateGenerator.Generate(1980, 2017)}', '{idKlienta}')");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}