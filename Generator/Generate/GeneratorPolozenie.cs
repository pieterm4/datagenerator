﻿using System;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Generator.DBConnection;
using Generator.Services;

namespace Generator.Generate
{
    public class GeneratorPolozenie : IGenerator
    {
        private const string Path = "C:\\Users\\piotr\\Desktop\\eh\\Generator\\Model\\wojMiasta.csv";
        private readonly GetDataFromCSV _getDataFromCsv;
        private readonly Random _random;

        private readonly SqlConnection _sqlConnecition;

        public GeneratorPolozenie()
        {
            _getDataFromCsv = new GetDataFromCSV();
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
            _random = new Random();
        }


        public async Task Generate()
        {
            try
            {
                var polozenia = await _getDataFromCsv.GetData(Path);
                using (_sqlConnecition)
                {
                    await _sqlConnecition.OpenAsync();

                    for (var i = 1; i <= 1000000; i++)
                    {
                        var index = _random.Next(0, polozenia.Count - 1);
                        var polozenie = polozenia.ElementAt(index);
                        var sqlCommand = new SqlCommand(
                            $"INSERT INTO Adres (Wojewodztwo, Powiat, Gmina, Miejscowosc, Ulica, NrDomu, NrMieszkania) VALUES ('{polozenie.Wojewodztwo}', '{polozenie.Powiat}', '{polozenie.Gmina}', '{polozenie.Miejscowosc}', '{polozenie.Ulica}', '{polozenie.NrDomu}', '{polozenie.NrMieszkania}')",
                            _sqlConnecition);
                        await sqlCommand.ExecuteNonQueryAsync();
                        Console.WriteLine(polozenie);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}