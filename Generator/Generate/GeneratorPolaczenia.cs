﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Generator.DBConnection;
using Generator.Model;
using Generator.Services;

namespace Generator.Generate
{
    public class GeneratorPolaczenia : IGenerator
    {
        private readonly Random _random;

        private readonly SqlConnection _sqlConnecition;

        public GeneratorPolaczenia()
        {
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
            _random = new Random();
        }

        public async Task Generate()
        {
            try
            {
                using (_sqlConnecition)
                {
                    await _sqlConnecition.OpenAsync();
                    for (var i = 0; i < 1000000; i++)
                    {
                        var idKlienta1 = 0;
                        var idKlienta2 = 0;

                        while (idKlienta1 == idKlienta2)
                        {
                            idKlienta1 = _random.Next(2, 1000000);
                            idKlienta2 = _random.Next(2, 1000000);
                        }

                        var date = DateGenerator.Generate(1980, 2017);
                        var czasTrwania = _random.Next(0, 999999999);

                        var idNadajnika1 = 0;
                        var idNadajnika2 = 0;
                        while (idNadajnika1 == idNadajnika2)
                        {
                            idNadajnika1 = _random.Next(1, 10000);
                            idNadajnika2 = _random.Next(1, 10000);
                        }

                        var zerwane = _random.Next(0, 2);


                        var polaczenie = new Polaczenia(idKlienta1, idKlienta2, date, czasTrwania.ToString(),
                            idNadajnika1,
                            idNadajnika2, zerwane);

                        var sqlCommand =
                            new SqlCommand(
                                $"INSERT INTO Polaczenia VALUES ({polaczenie.IdKlientDzwoni}, {polaczenie.IdKlientOdbiera}, '{polaczenie.Data}', '{polaczenie.CzasTrwania}', {polaczenie.IdNadajnikaDzwoni}, {polaczenie.IdNadajnikaOdbiera}, {polaczenie.CzyZerwane})",
                                _sqlConnecition);
                        await sqlCommand.ExecuteNonQueryAsync();
                        Console.WriteLine(i);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}