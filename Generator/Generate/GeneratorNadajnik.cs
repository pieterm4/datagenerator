﻿using System;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Generator.DBConnection;
using Generator.Model;

namespace Generator.Generate
{
    public class GeneratorNadajnik : IGenerator
    {
        private static Random _random;

        private static SqlConnection _sqlConnecition;

        public GeneratorNadajnik()
        {
            _sqlConnecition = new SqlConnection(Connection.ConnectionString);
            _random = new Random();
        }

        public async Task Generate()
        {
            try
            {
                using (_sqlConnecition)
                {
                    await _sqlConnecition.OpenAsync();

                    for (var i = 1; i <= 10000; i++)
                    {
                        var nadajnik = new Nadajnik($"Nadajnik {i}", _random.Next(100, 10000), i);

                        var sqlCommand =
                            new SqlCommand(
                                $"INSERT INTO Nadajnik VALUES ('{nadajnik.Nazwa}', {nadajnik.Moc}, {nadajnik.IdPolozenia})",
                                _sqlConnecition);
                        await sqlCommand.ExecuteNonQueryAsync();
                        Console.WriteLine(nadajnik);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}