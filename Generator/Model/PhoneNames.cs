﻿namespace Generator.Model
{
    public class PhoneNames
    {
        public string vendor { get; set; }
        public string model { get; set; }

        public override string ToString()
        {
            return $"{vendor}, {model}";
        }
    }
}