﻿namespace Generator.Model
{
    public class Klient
    {
        public Klient(string pesel, string imie, string nazwisko, int adres)
        {
            Pesel = pesel;
            Imie = imie;
            Nazwisko = nazwisko;
            Adres = adres;
        }

        public Klient()
        {
        }

        public string Pesel { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public int Adres { get; set; }

        public override string ToString()
        {
            return $"{Pesel}, {Imie}, {Nazwisko}, {Adres}";
        }
    }
}