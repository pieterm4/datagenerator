﻿namespace Generator.Model
{
    public class Nadajnik
    {
        public Nadajnik(string nazwa, float moc, int idPolozenia)
        {
            Nazwa = nazwa;
            Moc = moc;
            IdPolozenia = idPolozenia;
        }

        public string Nazwa { get; set; }
        public float Moc { get; set; }
        public int IdPolozenia { get; set; }

        public override string ToString()
        {
            return $"{Nazwa}, {Moc}, {IdPolozenia}";
        }
    }
}