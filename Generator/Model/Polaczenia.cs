﻿using System;

namespace Generator.Model
{
    public class Polaczenia
    {
        public Polaczenia(int idKlientDzwoni, int idKlientOdbiera, DateTime data, string czasTrwania,
            int idNadajnikaDzwoni, int idNadajnikaOdbiera, int czyZerwane)
        {
            IdKlientDzwoni = idKlientDzwoni;
            IdKlientOdbiera = idKlientOdbiera;
            Data = data;
            CzasTrwania = czasTrwania;
            IdNadajnikaDzwoni = idNadajnikaDzwoni;
            IdNadajnikaOdbiera = idNadajnikaOdbiera;
            CzyZerwane = czyZerwane;
        }

        public int IdKlientDzwoni { get; set; }
        public int IdKlientOdbiera { get; set; }
        public DateTime Data { get; set; }
        public string CzasTrwania { get; set; }
        public int IdNadajnikaDzwoni { get; set; }
        public int IdNadajnikaOdbiera { get; set; }
        public int CzyZerwane { get; set; }
    }
}