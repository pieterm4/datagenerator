﻿using System;

namespace Generator.Model
{
    public class Telefon
    {
        public string Imei { get; set; }
        public string Nazwa { get; set; }
        public string Model { get; set; }
        public DateTime DataZakupu { get; set; }
        public int IdKlienta { get; set; }
        public int IdSim { get; set; }
    }
}