﻿using System;

namespace Generator.Model
{
    public class Awarie
    {
        public Awarie(int idNadajnika, DateTime data, string uwagi)
        {
            IdNadajnika = idNadajnika;
            Data = data;
            Uwagi = uwagi;
        }

        public int IdNadajnika { get; set; }
        public DateTime Data { get; set; }
        public string Uwagi { get; set; }

        public override string ToString()
        {
            return $"{IdNadajnika}, {Data}, {Uwagi}";
        }
    }
}