﻿namespace Generator.Model
{
    public class Polozenie
    {
        public Polozenie(string wojewodztwo, string powiat, string gmina, string miejscowosc, string ulica,
            string nrDomu, string nrMieszkania)
        {
            Wojewodztwo = wojewodztwo;
            Powiat = powiat;
            Gmina = gmina;
            Miejscowosc = miejscowosc;
            Ulica = ulica;
            NrDomu = nrDomu;
            NrMieszkania = nrMieszkania;
        }

        public string Wojewodztwo { get; set; }
        public string Powiat { get; set; }
        public string Gmina { get; set; }
        public string Miejscowosc { get; set; }
        public string Ulica { get; set; }
        public string NrDomu { get; set; }
        public string NrMieszkania { get; set; }

        public override string ToString()
        {
            return $"{Wojewodztwo}, {Powiat}, {Gmina}, {Miejscowosc}, {Ulica}, {NrDomu}, {NrMieszkania}";
        }
    }
}